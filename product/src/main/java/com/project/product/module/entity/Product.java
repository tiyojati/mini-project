package com.project.product.module.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.project.product.module.controller.param.AddProductParam;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "t_product")
public class Product {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "product_name")
	private String productName;
	
	@Column(name = "product_code")
	private String productCode;
	
	@Column(name = "product_price")
	private BigDecimal productPrice;
	
	@Column(name = "store_name")
	private String storeName;
	
	@Column(name = "brand_name")
	private String brandName;
	
	@Column(name = "category_name")
	private String categoryName;
	
	@Lob
	@Column(name = "product_description", length = 512)
	private String productDescription;
	
	@Column(name = "created_date")
	private Date createdDate;
	
	@Column(name = "modified_date")
	private Date modifiedDate;
	
	public Product() {}
	
	public Product(AddProductParam addProductParam) {
		this.productName = addProductParam.getProductName();
		this.productCode = addProductParam.getProductCode();
		this.productPrice = addProductParam.getProductPrice();
		this.storeName = addProductParam.getStoreName();
		this.brandName = addProductParam.getBrandName();
		this.categoryName = addProductParam.getCategoryName();
		this.productDescription = addProductParam.getProductDescription();
		this.createdDate = new Date();
	}

}
