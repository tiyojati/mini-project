package com.project.product.module.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.product.module.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

	Product findProductById(Integer id);
	
}
