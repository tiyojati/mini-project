package com.project.product.module.controller.param;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AddProductParam {

	private String productName;
	private String productCode;
	private BigDecimal productPrice;
	private String storeName;
	private String brandName;
	private String categoryName;
	private String productDescription;
	
}
