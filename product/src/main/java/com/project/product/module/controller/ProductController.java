package com.project.product.module.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.project.product.base.BaseController;
import com.project.product.base.BaseException;
import com.project.product.base.ErrorResponse;
import com.project.product.base.RequestURL;
import com.project.product.module.controller.param.AddProductParam;
import com.project.product.module.entity.Product;
import com.project.product.module.service.ProductService;

@RestController
@RequestMapping(RequestURL.BaseUrl.API_BASE_URL)
public class ProductController extends BaseController {
	
	@Autowired ProductService productService;
	
	@GetMapping(RequestURL.BaseUrl.API_V1 + RequestURL.ProductUrl.PRODUCT_DETAIL_URL)
	public ResponseEntity<Object> detailProduct(@RequestParam Integer id) {
		Product response = new Product();
		try {
			response = productService.findProductById(id);
		} catch (BaseException e) {
			return responseBuilder.build(new ErrorResponse(this.message.getMessage(e.getCode())), e.getStatusCode());
		}
		return responseBuilder.build(response, HttpStatus.OK);
	}
	
	@PostMapping(RequestURL.BaseUrl.API_V1 + RequestURL.ProductUrl.PRODUCT_SAVE_URL)
	public ResponseEntity<Object> addProduct(AddProductParam addProductParam) {
		Product response = productService.addProduct(addProductParam);
		return responseBuilder.build(response, HttpStatus.OK);
	}

}
