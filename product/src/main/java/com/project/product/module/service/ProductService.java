package com.project.product.module.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.project.product.base.BaseException;
import com.project.product.common.enumeration.MessageCodes;
import com.project.product.common.utilities.Validator;
import com.project.product.module.controller.param.AddProductParam;
import com.project.product.module.entity.Product;
import com.project.product.module.repository.ProductRepository;

@Service("productService")
public class ProductService {

	@Autowired ProductRepository productRepository;
	
	public Product findProductById(Integer id) throws BaseException {
		Product product = productRepository.findProductById(id);
		if(Validator.isNullOrEmpty(product))
			throw new BaseException(MessageCodes.PRODUCT_NOT_FOUND, HttpStatus.BAD_REQUEST);
		
		return product;
	}
	
	public Product addProduct(AddProductParam addProductParam) {
		Product product = new Product(addProductParam);	
		return productRepository.save(product);
	}
}
