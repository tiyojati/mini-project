package com.project.product.common.enumeration;

public enum MessageCodes {
	NO_PERMISSION_ACCESS("no.permission.access"),
	PRODUCT_NOT_FOUND("product.not.found"),

	;
	
	private String value;
	
	private MessageCodes(String value) {
		this.value = value;
	}
	
	public String value() {
		return this.value;
	}
}
