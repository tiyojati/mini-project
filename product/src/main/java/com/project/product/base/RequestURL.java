package com.project.product.base;

public class RequestURL {
	
	public class BaseUrl{
		/* API Version */
		public static final String API_V1 = "/v1";
		/* Base URL */
		public static final String API_BASE_URL = "/api";
	}
	
	public class ProductUrl{
		public static final String PRODUCT_DETAIL_URL = "/product/detail";
		public static final String PRODUCT_SAVE_URL = "/product/add";
	}
	
}
