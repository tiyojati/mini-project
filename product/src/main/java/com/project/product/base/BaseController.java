package com.project.product.base;

import org.springframework.beans.factory.annotation.Autowired;

public class BaseController {
	
	@Autowired
	protected BaseMessage message;
	
	@Autowired
	protected ResponseBuilder responseBuilder;
	
}
