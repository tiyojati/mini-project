package com.project.product.base;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;


@Component
public class ResponseBuilder {

	/**
	 * Create a new {@code ResponseEntity} with the given data and status code.
	 * @param data the response data
	 * @param httpStatus the status code
	 */
	public ResponseEntity<Object> build(Object data, HttpStatus httpStatus) {
		return new ResponseEntity<Object>(new BaseResponse(data, httpStatus), httpStatus);
	}

}
