package com.project.member.module.service;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.project.member.base.BaseException;
import com.project.member.common.enumeration.MessageCodes;
import com.project.member.common.utilities.CommonUtil;
import com.project.member.common.utilities.GenerateUtil;
import com.project.member.common.utilities.LoginType;
import com.project.member.common.utilities.Validator;
import com.project.member.module.controller.param.MemberRegistrationParam;
import com.project.member.module.controller.param.UpdateProfileParam;
import com.project.member.module.entity.Member;
import com.project.member.module.entity.MemberLoginToken;
import com.project.member.module.entity.MemberProfile;
import com.project.member.module.repository.MemberLoginTokenRepository;
import com.project.member.module.repository.MemberProfileRepository;
import com.project.member.module.repository.MemberRepository;

@Service("memberService")
public class MemberService {
	
	@Autowired MemberRepository memberRepository;
	
	@Autowired MemberProfileRepository memberProfileRepository;
	
	@Autowired MemberLoginTokenRepository memberLoginTokenRepository;
	
	public Member register(MemberRegistrationParam memberRegistrationParam) throws BaseException {
		if(Validator.isValidRegistrationForm(memberRegistrationParam)) {
			Member member = new Member(memberRegistrationParam);
			
			int emailcount = memberRepository.countByEmail(member.getEmail());
			if(emailcount > 1)
				throw new BaseException(MessageCodes.REGISTRATION_EMAIL_IS_DUPLICATE, HttpStatus.BAD_REQUEST);
			int mobilecount = memberRepository.countByMobileNumber(member.getMobileNumber());
			if(mobilecount > 1)
				throw new BaseException(MessageCodes.REGISTRATION_MOBILE_NUMBER_IS_DUPLICATE, HttpStatus.BAD_REQUEST);
			
			member = memberRepository.save(member);
			if(Validator.isNotNullOrEmpty(member.getId())) {
				MemberProfile memberProfile = new MemberProfile();
				memberProfile.setMemberId(member.getId());
				memberProfile.setCreatedDate(new Date());
				memberProfileRepository.save(memberProfile);
			}
			return member;
		}
		return null;
	}
	
	public MemberLoginToken login(String memberAccount, String password) throws BaseException {
		Member member = new Member();
		LoginType.Channel loginChannel = LoginType.identityAccountType(memberAccount);
		switch (loginChannel) {
		case EMAIL:
            member = memberRepository.findByEmail(memberAccount);
            break;
        case MOBILE:
            member = memberRepository.findByMobileNumber(CommonUtil.normalizeMobileNumber(memberAccount));
            break;
        default:
			break;
		}
		
		if(Validator.isNullOrEmpty(member)) {
			throw new BaseException(MessageCodes.MEMBER_NOT_FOUND, HttpStatus.BAD_REQUEST);
		}
		
		if(!StringUtils.equals(member.getPassword(), password)) {
			throw new BaseException(MessageCodes.PASSWORD_IS_INCORRECT, HttpStatus.BAD_REQUEST);
		}
		
		return this.saveLoginToken(member);
	}

	public MemberProfile getMemberProfile(Integer memberId) {
		return memberProfileRepository.findByMemberId(memberId);
	}
	
	public MemberProfile updateMemberProfile(UpdateProfileParam updateProfileParam, Integer memberId) {
		MemberProfile profile = new MemberProfile(updateProfileParam);
		memberProfileRepository.updateMemberProfileByMemberId(profile, memberId, new Date());
		return this.getMemberProfile(memberId);
	}
	
	public MemberLoginToken saveLoginToken(Member member) {
		String memberToken = GenerateUtil.generateMemberToken(member.getId());
		Date expired = GenerateUtil.generateMemberTokenExpired(new Date());

		MemberLoginToken mlToken = new MemberLoginToken(member.getId(), memberToken, expired);
		mlToken = memberLoginTokenRepository.save(mlToken);

		return mlToken;
	}
	
	public Member findMemberByToken(String token) {
		MemberLoginToken mlToken = memberLoginTokenRepository.findByToken(token);
		if(Validator.isNullOrEmpty(mlToken)) {
			return null;
		}
		return memberRepository.findById(mlToken.getMemberId()).get();
	}
}
