package com.project.member.module.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.member.module.entity.Member;

@Repository
public interface MemberRepository extends JpaRepository<Member, Integer>{
	
	Member findByEmail(String email);
	
	Member findByMobileNumber(String mobileNumber);
	
	int countByEmail(String email);
	
	int countByMobileNumber(String mobileNumber);

}
