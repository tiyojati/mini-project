package com.project.member.module.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name="t_member_login_token", indexes = { @Index(name = "token_memberid", columnList = "member_id", unique = false) })
public class MemberLoginToken {
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "member_id")
    private Integer memberId;
	
	@Column(name = "token")
    private String token;
	
	@Column(name = "created_date")
    private Date createdDate;
	
	@Column(name = "expired_date")
    private Date expiredDate;
	
	public MemberLoginToken() {}
	
	public MemberLoginToken(Integer memberId, String memberToken, Date expiredDate) {
		this.memberId = memberId;
		this.token = memberToken;
		this.createdDate = new Date();
		this.expiredDate = expiredDate;
	}

}
