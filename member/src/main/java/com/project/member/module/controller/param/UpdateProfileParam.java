package com.project.member.module.controller.param;

import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
public class UpdateProfileParam {

	private String firstName;
	private String lastName;
	private String gender;
	private String dateOfBirth;
	
}
