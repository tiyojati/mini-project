package com.project.member.module.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.project.member.common.utilities.CommonUtil;
import com.project.member.module.controller.param.MemberRegistrationParam;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "t_member")
public class Member {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "email_verified")
	private boolean emailVerified;
	
	@Column(name = "mobile_number")
	private String mobileNumber;
	
	@Column(name = "mobile_verified")
	private boolean mobileVerified;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "created_date")
	private Date createdDate;
	
	@Column(name = "modified_date")
	private Date modifiedDate;
	
	public Member() {}
	
	public Member(MemberRegistrationParam memberRegistrationParam) {
		this.email = memberRegistrationParam.getEmail();
		this.emailVerified = false;
		this.mobileNumber = CommonUtil.normalizeMobileNumber(memberRegistrationParam.getMobileNumber());
		this.mobileVerified = false;
		this.password = memberRegistrationParam.getPassword();
		this.createdDate = new Date();
	}
	
}
