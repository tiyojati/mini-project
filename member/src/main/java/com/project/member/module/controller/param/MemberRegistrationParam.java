package com.project.member.module.controller.param;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MemberRegistrationParam {
	
	private String email;
	private String mobileNumber;
	private String password;
	
}
