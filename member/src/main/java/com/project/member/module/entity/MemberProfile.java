package com.project.member.module.entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Index;

import com.project.member.module.controller.param.UpdateProfileParam;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "t_member_profile", indexes = { @Index(name = "profile_memberid", columnList = "member_id", unique = true) })
public class MemberProfile {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "member_id")
	private Integer memberId;
	
	@Column(name = "first_name")
	private String firstName;
	
	@Column(name = "last_name")
	private String lastName;
	
	@Column(name = "gender")
	private String gender;
	
	@Column(name = "date_of_birth")
	@Temporal(TemporalType.DATE)
	private Date dateOfBirth;
	
	@Column(name = "created_date")
	private Date createdDate;
	
	@Column(name = "modified_date")
	private Date modifiedDate;
	
	public MemberProfile() {}
	
	public MemberProfile(UpdateProfileParam updateProfileParam) {
		this.firstName = updateProfileParam.getFirstName();
		this.lastName = updateProfileParam.getLastName();
		this.gender = updateProfileParam.getGender();
		
		try {
			this.dateOfBirth = new SimpleDateFormat("dd-MM-yyyy").parse(updateProfileParam.getDateOfBirth());
		} catch (ParseException e) {
			// TODO Do Nothing
		}
	}
	
}
