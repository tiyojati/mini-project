package com.project.member.module.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.member.base.BaseController;
import com.project.member.base.BaseException;
import com.project.member.base.ErrorResponse;
import com.project.member.base.RequestURL;
import com.project.member.common.enumeration.MessageCodes;
import com.project.member.common.utilities.Validator;
import com.project.member.module.controller.param.MemberRegistrationParam;
import com.project.member.module.controller.param.UpdateProfileParam;
import com.project.member.module.entity.Member;
import com.project.member.module.entity.MemberLoginToken;
import com.project.member.module.entity.MemberProfile;
import com.project.member.module.service.MemberService;

@RestController
@RequestMapping(RequestURL.BaseUrl.API_BASE_URL)
public class MemberController extends BaseController{
	
	@Autowired MemberService memberService;
	
	@PostMapping(RequestURL.BaseUrl.API_V1 + RequestURL.MemberUrl.REGISTER_URL)
	public ResponseEntity<Object> register(MemberRegistrationParam memberRegistrationParam) {
		Map<String, String> response = new HashMap<String, String>();
		try {
			Member member = memberService.register(memberRegistrationParam);
			if(Validator.isNullOrEmpty(member)) {
				response.put("message", this.message.getMessage(MessageCodes.REGISTRATION_ERROR));
			}else {
				response.put("message", this.message.getMessage(MessageCodes.REGISTRATION_SUCCESS));
			}
		} catch (BaseException e) {
			return responseBuilder.build(new ErrorResponse(this.message.getMessage(e.getCode())), e.getStatusCode());
		}

		return responseBuilder.build(response, HttpStatus.OK);
	}

	@PostMapping(RequestURL.BaseUrl.API_V1 + RequestURL.MemberUrl.LOGIN_URL)
	public ResponseEntity<Object> login(String memberAccount, String password) {
		MemberLoginToken response = new MemberLoginToken();
		try {
			response = memberService.login(memberAccount, password);
		} catch (BaseException e) {
			return responseBuilder.build(new ErrorResponse(this.message.getMessage(e.getCode())), e.getStatusCode());
		}

		return responseBuilder.build(response, HttpStatus.OK);
	}
	
	@GetMapping(RequestURL.BaseUrl.API_V1 + RequestURL.MemberUrl.PROFILE_URL)
	public ResponseEntity<Object> profile(HttpServletRequest request) {
		String token = getMemberToken(request);
		if(Validator.isNullOrEmpty(token)) {
			return responseBuilder.build(new ErrorResponse(this.message.getMessage(MessageCodes.NO_PERMISSION_ACCESS)), HttpStatus.FORBIDDEN);
		}
		Member member = memberService.findMemberByToken(token);
		if(Validator.isNullOrEmpty(member)) {
			return responseBuilder.build(new ErrorResponse(this.message.getMessage(MessageCodes.NO_PERMISSION_ACCESS)), HttpStatus.FORBIDDEN);
		}
		MemberProfile response = memberService.getMemberProfile(member.getId());

		return responseBuilder.build(response, HttpStatus.OK);
	}
	
	@PostMapping(RequestURL.BaseUrl.API_V1 + RequestURL.MemberUrl.EDIT_PROFILE_URL)
	public ResponseEntity<Object> editProfile(UpdateProfileParam updateProfileParam, HttpServletRequest request) {
		String token = getMemberToken(request);
		if(Validator.isNullOrEmpty(token)) {
			return responseBuilder.build(new ErrorResponse(this.message.getMessage(MessageCodes.NO_PERMISSION_ACCESS)), HttpStatus.FORBIDDEN);
		}
		Member member = memberService.findMemberByToken(getMemberToken(request));
		if(Validator.isNullOrEmpty(member)) {
			return responseBuilder.build(new ErrorResponse(this.message.getMessage(MessageCodes.NO_PERMISSION_ACCESS)), HttpStatus.FORBIDDEN);
		}
		MemberProfile response = memberService.updateMemberProfile(updateProfileParam, member.getId());

		return responseBuilder.build(response, HttpStatus.OK);
	}
}
