package com.project.member.module.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.project.member.module.entity.MemberProfile;

@Repository
public interface MemberProfileRepository extends JpaRepository<MemberProfile, Integer>{
	
	MemberProfile findByMemberId(Integer memberId);
	
	@Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = 
    		"update t_member_profile set "
    		+ "first_name = :#{#profile.firstName}, "
            + "last_name = :#{#profile.lastName}, "
            + "gender = :#{#profile.gender},"
            + "date_of_birth = :#{#profile.dateOfBirth},"
            + "modified_date = :modifiedDate "
            + "where member_id = :memberId"
            , nativeQuery = true)
    public int updateMemberProfileByMemberId(
            @Param("profile") MemberProfile profile,
            @Param("memberId") Integer memberId,
            @Param("modifiedDate") Date modifiedDate
            );

}
