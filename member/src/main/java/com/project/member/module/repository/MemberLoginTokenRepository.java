package com.project.member.module.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.member.module.entity.MemberLoginToken;

@Repository
public interface MemberLoginTokenRepository extends JpaRepository<MemberLoginToken, Integer> {
	
	MemberLoginToken findByToken(String token);
	
}
