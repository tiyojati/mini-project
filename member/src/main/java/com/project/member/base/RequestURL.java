package com.project.member.base;

public class RequestURL {
	
	public class BaseUrl{
		/* API Version */
		public static final String API_V1 = "/v1";
		/* Base URL */
		public static final String API_BASE_URL = "/api";
	}
	
	public class MemberUrl{
		public static final String REGISTER_URL = "/member/register";
		public static final String LOGIN_URL = "/member/login";
		public static final String PROFILE_URL = "/member/profile";
		public static final String EDIT_PROFILE_URL = "/member/profile/edit";
	}
	
}
