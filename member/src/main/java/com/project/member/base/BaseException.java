package com.project.member.base;

import org.springframework.http.HttpStatus;

import com.project.member.common.enumeration.MessageCodes;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BaseException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9096677093257630458L;
	
	private MessageCodes code;
	private MessageCodes title;
	private HttpStatus statusCode;
	private Object[] args;
	
	public BaseException(MessageCodes code, HttpStatus statusCode) {
		super();
		this.code = code;
		this.statusCode = statusCode;
	}
	
	public BaseException(MessageCodes code, Object[] args, HttpStatus statusCode) {
		super();
		this.code = code;
		this.statusCode = statusCode;
		this.args = args;
	}
	
	public BaseException(MessageCodes code, MessageCodes title, HttpStatus statusCode) {
		super();
		this.code = code;
		this.title = title;
		this.statusCode = statusCode;
	}
	
	public BaseException(MessageCodes code, MessageCodes title, Object[] args, HttpStatus statusCode) {
		super();
		this.code = code;
		this.title = title;
		this.statusCode = statusCode;
		this.args = args;
	}

}
