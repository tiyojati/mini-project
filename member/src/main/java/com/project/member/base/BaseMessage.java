package com.project.member.base;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import com.project.member.common.enumeration.MessageCodes;


@Component
public class BaseMessage {

	@Autowired(required = false)
	private MessageSource messageSource;

	public String getMessage(MessageCodes code) {
		return messageSource.getMessage(code.value(), null, Locale.ROOT);
	}
	
	public String getMessage(String code) {
		return messageSource.getMessage(code, null, Locale.ROOT);
	}

	public String getMessage(MessageCodes code, Object...args) {
		return messageSource.getMessage(code.value(), args, Locale.ROOT);
	}
	
}
