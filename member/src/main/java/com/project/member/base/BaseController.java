package com.project.member.base;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

public class BaseController {
	
	private static final String MEMBER_TOKEN				= "member-token";
	
	@Autowired
	protected BaseMessage message;
	
	@Autowired
	protected ResponseBuilder responseBuilder;
	
	public String getMemberToken(HttpServletRequest request){
		return request.getHeader(MEMBER_TOKEN);
	}

}
