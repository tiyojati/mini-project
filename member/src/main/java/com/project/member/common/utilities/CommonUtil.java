package com.project.member.common.utilities;

import org.apache.commons.lang3.StringUtils;

public class CommonUtil {
	
	public static String normalizeMobileNumber(String mobileNumber){
        if(StringUtils.isBlank(mobileNumber)){
            return mobileNumber;
        }
        mobileNumber = mobileNumber.trim();
        if("0".equals(StringUtils.left(mobileNumber, 1))){
            return StringUtils.right(mobileNumber, mobileNumber.length() - 1);
        } else {
            return mobileNumber;
        }
    }

}
