package com.project.member.common.enumeration;

public enum MessageCodes {
	NO_PERMISSION_ACCESS("no.permission.access"),
	MEMBER_NOT_FOUND("member.not.found"),
	
	REGISTRATION_RQ_NULL("registration.rq.null"),
	REGISTRATION_ERROR("registration.error"),
	REGISTRATION_SUCCESS("registration.success"),
	REGISTRATION_PLEASE_INPUT_EMAIL("registration.please.input.email"),
	REGISTRATION_EMAIL_IS_DUPLICATE("registration.email.duplicate"),
	REGISTRATION_PLEASE_INPUT_MOBILE_NUMBER("registration.please.input.mobile.number"),
	REGISTRATION_MOBILE_NUMBER_IS_DUPLICATE("registration.mobile.number.duplicate"),
	REGISTRATION_PLEASE_INPUT_PASSWORD("registration.please.input.password"),
	REGISTRATION_EMAIL_NOT_VALID("registration.email.not.valid"),
	REGISTRATION_PASSWORD_NOT_VALID("registration.password.not.valid"),
	REGISTRATION_MOBILE_NUMBER_NOT_VALID("registration.mobile.number.not.valid"),
	
	PASSWORD_IS_INCORRECT("password.is.incorrect"),

	;
	
	private String value;
	
	private MessageCodes(String value) {
		this.value = value;
	}
	
	public String value() {
		return this.value;
	}
}
