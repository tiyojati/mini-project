package com.project.member.common.utilities;

import java.util.regex.Pattern;

public class LoginType {
	public static final Pattern MAIL_PATTERN = Pattern
    .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
	public static final Pattern MOBILE_PATTERN = Pattern
    .compile("^[0-9]{8,12}$");

	public static enum Channel{
		EMAIL, MOBILE
	}
	
	public static Channel identityAccountType(String memberAccount) {
        if (MAIL_PATTERN.matcher(memberAccount).matches()) {
            return Channel.EMAIL;
        } else if (MOBILE_PATTERN.matcher(memberAccount).matches()) {
            return Channel.MOBILE;
        } else {
            return null;
        }
    }
}
