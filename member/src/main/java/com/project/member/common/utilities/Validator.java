package com.project.member.common.utilities;

import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.http.HttpStatus;

import com.project.member.base.BaseException;
import com.project.member.common.enumeration.MessageCodes;
import com.project.member.module.controller.param.MemberRegistrationParam;

public final class Validator {

	private Validator(){}

	public static boolean isNullOrEmpty(Object value){
		if (null == value){
			return true;
		}

		if (value instanceof String){
			return value.toString().trim().length() <= 0;
		}

		if (value instanceof Collection){
			return ((Collection<?>) value).isEmpty();
		}

		if (value instanceof Map){
			return ((Map<?, ?>) value).isEmpty();
		}

		if (value instanceof Enumeration){
			return !((Enumeration<?>) value).hasMoreElements();
		}

		if (value instanceof Iterator){
			return !((Iterator<?>) value).hasNext();
		}

		boolean arrayFlag = arrayIsNullOrEmpty(value);
		if (arrayFlag){
			return true;
		}
		return false;
	}

	public static boolean isNotNullOrEmpty(Object value){
		return !isNullOrEmpty(value);
	}

	private static boolean arrayIsNullOrEmpty(Object value){
		if (value instanceof Object[]){
			return ((Object[]) value).length == 0;
		}

		if (value instanceof int[]){
			return ((int[]) value).length == 0;
		}

		if (value instanceof long[]){
			return ((long[]) value).length == 0;
		}

		if (value instanceof float[]){
			return ((float[]) value).length == 0;
		}

		if (value instanceof double[]){
			return ((double[]) value).length == 0;
		}

		if (value instanceof char[]){
			return ((char[]) value).length == 0;
		}

		if (value instanceof boolean[]){
			return ((boolean[]) value).length == 0;
		}

		if (value instanceof byte[]){
			return ((byte[]) value).length == 0;
		}

		if (value instanceof short[]){
			return ((short[]) value).length == 0;
		}
		return false;
	}
	
	public static boolean isValidRegistrationForm(MemberRegistrationParam memberRegistrationParam) throws BaseException {
		if(isNullOrEmpty(memberRegistrationParam)){
			throw new BaseException(MessageCodes.REGISTRATION_RQ_NULL, HttpStatus.BAD_REQUEST);
		}
		
		if(isNullOrEmpty(memberRegistrationParam.getEmail())) {
			throw new BaseException(MessageCodes.REGISTRATION_PLEASE_INPUT_EMAIL, HttpStatus.BAD_REQUEST);
		}else if(!isValidEmail(memberRegistrationParam.getEmail())) {
			throw new BaseException(MessageCodes.REGISTRATION_EMAIL_NOT_VALID, HttpStatus.BAD_REQUEST);
		}
		
		if(isNullOrEmpty(memberRegistrationParam.getMobileNumber())) {
			throw new BaseException(MessageCodes.REGISTRATION_PLEASE_INPUT_MOBILE_NUMBER, HttpStatus.BAD_REQUEST);
		}else if(!isValidPhoneNumber(memberRegistrationParam.getMobileNumber())) {
			throw new BaseException(MessageCodes.REGISTRATION_MOBILE_NUMBER_NOT_VALID, HttpStatus.BAD_REQUEST);
		}
		
		if(isNullOrEmpty(memberRegistrationParam.getPassword())) {
			throw new BaseException(MessageCodes.REGISTRATION_PLEASE_INPUT_PASSWORD, HttpStatus.BAD_REQUEST);
		}else if(!isValidPassword(memberRegistrationParam.getPassword())) {
			throw new BaseException(MessageCodes.REGISTRATION_PASSWORD_NOT_VALID, HttpStatus.BAD_REQUEST);
		}
		
		
		return true;
	}

	public static boolean isValidPassword(String password) {
		String regex = "^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*()+=_-]).{6,16}$";

		Pattern p = Pattern.compile(regex);

		if (password == null) {
			return false;
		}

		Matcher m = p.matcher(password);

		return m.matches();
	}

	public static boolean isValidEmail(String email) {
		String regex = "^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";

		Pattern p = Pattern.compile(regex);

		if (email == null) {
			return false;
		}

		Matcher m = p.matcher(email);

		return m.matches();
	}

	public static boolean isValidPhoneNumber(String mobileNumber) {
		String regex = "^[0-9]{10,15}$";

		Pattern p = Pattern.compile(regex);

		if (mobileNumber == null) {
			return false;
		}

		Matcher m = p.matcher(mobileNumber);

		return m.matches();
	}

	public static boolean isAlphabetWithSpace(String text) {
		String regex = "^[a-zA-Z ]{3,30}$";

		Pattern p = Pattern.compile(regex);

		if (text == null) {
			return false;
		}

		Matcher m = p.matcher(text);

		return m.matches();
	}
	
}
