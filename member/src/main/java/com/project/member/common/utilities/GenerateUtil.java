package com.project.member.common.utilities;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class GenerateUtil {
	public static final String UNDERSCORE_SEPARATOR = "_";

	public static String generateMemberToken(Integer memberId){
		// Prepare parameter
		StringBuilder utoken = new StringBuilder("");
		utoken.append(memberId.toString());
		utoken.append(UNDERSCORE_SEPARATOR);
		utoken.append(System.currentTimeMillis());
		
		// Hash the data
		String result = "";
		try {
			result = HmacSHA1Util.encrypt(utoken.toString(), memberId.toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public static Date generateMemberTokenExpired(Date d){
		Calendar today = Calendar.getInstance();
		today.setTime(d);
		today.add(Calendar.DATE, Integer.parseInt("1"));
		return today.getTime();
	}
	
	public static class HmacSHA1Util {

		private static final String HMAC_SHA1 = "HmacSHA1";

		private static final String ENCODING = "UTF-8";

		public static String encrypt(String encryptText, String encryptKey) throws Exception {
			byte[] data = encryptKey.getBytes(ENCODING);
			SecretKey secretKey = new SecretKeySpec(data, HMAC_SHA1);
			Mac mac = Mac.getInstance(HMAC_SHA1);
			mac.init(secretKey);

			byte[] text = encryptText.getBytes(ENCODING);
			return toString(mac.doFinal(text));
		}

		private static String toString(byte[] cipherValue) {
			BigInteger bigInteger = new BigInteger(1, cipherValue);

			String cipher = bigInteger.toString(16);

			int paddingLength = (cipherValue.length * 2) - cipher.length();

			if (paddingLength > 0) {

				return String.format("%0" + paddingLength + "d", 0) + cipher;
			} else {

				return cipher;
			}
		}
	}
}
